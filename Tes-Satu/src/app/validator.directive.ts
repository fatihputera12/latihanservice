import { Directive } from '@angular/core';
import { AbstractControl, ValidatorFn, NG_VALIDATORS } from '@angular/forms';

function NoWhitespaceValidator(): ValidatorFn {

  return (control: AbstractControl): { [key: string]: any } => {

    let isWhitespace = (control.value || '').trim().length === 0;
    let isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': 'value is only whitespace' };

  };
}

function patternValidator(regex = new RegExp('^[0-9]*$')): ValidatorFn {

  return (control: AbstractControl): { [key: string]: any } => {

    let text = regex.test(control.value);
    return text ? null : { 'pattern': {value: control.value} };

  };
}

@Directive({
  selector: '[appValidator]',
  providers: [{ provide: NG_VALIDATORS, useExisting: ValidatorDirective, multi: true }]
})
export class ValidatorDirective {

  constructor() { }
  private valFn = NoWhitespaceValidator();
    validate(control: AbstractControl): { [key: string]: any } {
        return this.valFn(control);
    }


}
@Directive({
  selector: '[appPatt]',
  providers: [{ provide: NG_VALIDATORS, useExisting: PatternDirective, multi: true }]
})
export class PatternDirective {

  constructor() { }

  private valFn = patternValidator();
    validate(control: AbstractControl): { [key: string]: any } {
        return this.valFn(control);
    }

}

