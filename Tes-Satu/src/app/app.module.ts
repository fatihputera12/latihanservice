import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {AppComponent} from './app.component';
import {MultiSelectModule} from 'primeng/multiselect';
import {TableModule} from 'primeng/table';
import {LightboxModule} from 'primeng/lightbox';
import {ReactiveFormsModule} from '@angular/forms';
import {ButtonModule} from 'primeng/button';
import {RadioButtonModule} from 'primeng/radiobutton';
import {DropdownModule} from 'primeng/dropdown';
import {DialogModule} from 'primeng/dialog';
import {BrowserModule} from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { DataTableComponent } from './data-table/data-table.component';
import { InputFormComponent } from './input-form/input-form.component';
import {InputTextModule} from 'primeng/inputtext';
import { ValidatorDirective } from './validator.directive';
@NgModule({
    declarations: 
    [   AppComponent,
        DataTableComponent,
        InputFormComponent,
        ValidatorDirective
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        TableModule,
        FormsModule,
        MultiSelectModule,
        LightboxModule,
        ReactiveFormsModule,
        ButtonModule,
        RadioButtonModule,
        DropdownModule,
        DialogModule,
        HttpClientModule,
        InputTextModule
    ],
    providers: [],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule { }
