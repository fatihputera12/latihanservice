import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Subject,Observable,of } from 'rxjs';
import {Mobil} from './data';
import {CAR} from './mock-data';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};
@Injectable({
  providedIn: 'root'
})
export class AppService {
  cars= CAR;
  dataSource= new Subject<any>();
  data=this.dataSource.asObservable();
  
  dataSend= new Subject<any>();
  dataS=this.dataSend.asObservable();
  
  displaySource = new Subject<boolean>();
  dataDisplay = this.displaySource.asObservable();

  constructor(){
    this.dataSource.next(this.cars);
  }

  addMobil($data){
    this.cars.push($data);
    this.dataSource.next(this.cars);
  }

  getMobil(): Observable<Mobil[]> {
    return of(this.cars);
  }

  sendMobil(car){
    this.dataSend.next(car);
  }

  editData(car) {
    const i = this.cars.indexOf(this.cars.find(cr => cr.vin === car.vin));
    this.cars[i] = car;
  }

  display () {
    this.displaySource.next(true);
  }
  
}
