import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup,FormControl,Validators } from '@angular/forms';
import { FormArray } from '@angular/forms';
import {SelectItem} from 'primeng/api';
import {AppService} from '../app.service';
@Component({
  selector: 'app-input-form',
  templateUrl: './input-form.component.html',
  
})
export class InputFormComponent implements OnInit {
  
  constructor(private appService: AppService) { }
  typez:any[];
  display:boolean;
  status=null;
  ngOnInit() {
    this.typez= [
      {label: 'SUV', value: 'SUV'},
      {label: 'Mini Bus', value: 'Mini Bus'},
      {label: 'Jeep', value: 'Jeep'},
      {label: 'Sedan', value: 'Sedan'},
      {label: 'Van', value: 'Van'},
      {label: 'Sport', value: 'Sport'}
    ];
    this.appService.dataSend.subscribe( dt => 
      { this.formInput.patchValue(dt);
        this.status=true;
      });
      
      this.appService.dataDisplay.subscribe( st => {
        this.display = st;
      });
    }
    
    formInput = new FormGroup({
      vin: new FormControl(''),
      brand: new FormControl('',(Validators.pattern('^[a-zA-Z0-9]*$'))),
      year: new FormControl(''),
      color: new FormControl(''),
      price: new FormControl(''),
      type: new FormControl('')
    });
    
    save() {
      if (this.status) {
        this.appService.editData(this.formInput.value);
      } else {
        this.appService.addMobil(this.formInput.value);
      }
      this.status = null;
      this.formInput.reset();
      this.display = false;
    }
  }
  