import { Component, OnInit } from '@angular/core';
import {AppService} from '../app.service';
import {CAR} from '../mock-data';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  
})
export class DataTableComponent implements OnInit {
  
  constructor(private appService: AppService) { }
  cars= CAR;
  localData:any;
  
  ngOnInit() {
    this.appService.data.subscribe(car => this.localData = car);
  }

  cols: any = [
    { field: 'vin', header: 'Vin' },
    { field: 'year', header: 'Year' },
    { field: 'brand', header: 'Brand' },
    { field: 'color', header: 'Color' },
    { field: 'type', header: 'Type' },
    { field: 'price', header: 'Price ($USD)' }
  ];

  delete(i) {
    this.cars.splice(i,1);
  }

  edit(car){
    this.appService.sendMobil(car);
    this.appService.display();
  }
  
  display() {
    this.appService.display();
  }

}
