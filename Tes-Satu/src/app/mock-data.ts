import { Mobil } from './data';

export const CAR : Mobil[] = [  
    {"vin":"a1653d4d","brand":"VW","year":1995,"color":"White","type":"Van","price":10000},
    {"vin":"ddeb9b10","brand":"Mercedes","year":1985,"color":"Green","type":"Sport","price":25000},
    {"vin":"d8ebe413","brand":"Jaguar","year":1985,"color":"Silver","type":"Sport","price":30000},
    {"vin":"aab227b7","brand":"Audi","year":1990,"color":"Black","type":"Sport","price":12000},
    {"vin":"b69e6f5c","brand":"Jaguar","year":1993,"color":"Yellow","type":"sport","price":16000},
    {"vin":"ead9bf1d","brand":"Fiat","year":1968,"color":"Maroon","type":"sport","price":43000},
    {"vin":"bc58113e","brand":"Renault","year":1981,"color":"Silver","type":"sport","price":36000},
    {"vin":"2989d5b1","brand":"Honda","year":2006,"color":"Blue","type":"SUV","price":240000},
    {"vin":"c243e3a0","brand":"Fiat","year":1990,"color":"Maroon","type":"sport","price":15000},
];