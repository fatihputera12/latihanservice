export class Mobil{
    vin:string;
    brand:string;
    year:number;
    color:string;
    type:string;
    price:number;
}